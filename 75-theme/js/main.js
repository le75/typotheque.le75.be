var _BODY;
var _BG;
var _NAV;
var _NAV_SUMMARIES;
var _COLOR;
var _ABOUT;
var _TRYFONTS;

var title = document.querySelector("nav h1");
var about = document.getElementById("about");
var aboutClose = about.querySelector(".close");
var inputs = document.querySelectorAll('.sample-text');
var tryFonts = document.getElementById('try-fonts');
var tryFontsText = tryFonts.querySelector('summary');
var mainFonts = document.getElementById('main-fonts');
var tools = document.getElementById('tester');
var sizer = tools.querySelector('.size');

var filters = document.querySelectorAll(".filter");
var tags = document.querySelectorAll(".value");
var fonts = document.querySelectorAll(".font");
var windowW = window.innerWidth;

changeFontSize(inputs);

tryFonts.addEventListener('click', function() {
  if (!mainFonts.classList.contains('try-mode')) {
    tryFontsText.innerHTML = 'Preview';
    mainFonts.classList.add('try-mode');
    sizer.classList.add('visible');
  } else {
    tryFontsText.innerHTML = 'Try fonts';
    mainFonts.classList.remove('try-mode');
    sizer.classList.remove('visible');
  }
})

title.addEventListener("click", function () {
  if (about.classList.contains("hidden")) {
    about.classList.remove("hidden");
  } else {
    about.classList.add("hidden");
  }
});

aboutClose.addEventListener("click", function () {
  about.classList.add("hidden");
});

inputs.forEach(function(input){
  
  input.addEventListener("input", function(){
    var test = input.value;
		inputs.forEach(function(input){
			input.value = test;
		});
	});

});

if (windowW <= 600) {
  mainFonts.classList.add('try-mode');
  sizer.classList.add('visible');
}

window.addEventListener('resize', function() {
  windowW = window.innerWidth;
  if (windowW <= 600) {
    tryFontsText.innerHTML = 'Preview';
    mainFonts.classList.add('try-mode');
    sizer.classList.add('visible');
  } else if (windowW > 600) {
    tryFontsText.innerHTML = 'Try fonts';
    mainFonts.classList.remove('try-mode');
    sizer.classList.remove('visible');
  }
})


function changeFontSize(demos){

  var fontSizeInput = document.getElementById("fontSize");

  fontSizeInput.addEventListener("input", function(){

    var newSize = this.value;

    demos.forEach(function(demo){

      demo.style.fontSize=newSize+"px";

    });

  });
}

function loadColor() {
  var cookies = document.cookie;
  if (cookies.length > 0) {
		cookies = cookies.split(";");
    cookies.forEach(function (cookie) {
      name = cookie.split("=")[0].replace(" ", "");
      value = cookie.split("=")[1].replace(" ", "");
			console.log(name)
			console.log(value)
			document.documentElement.style.setProperty(name, value);
      // _BODY[0].style[name] = value;
      // _NAV[0].style[name] = value;
      // _ABOUT[0].style[name] = value;
      //
      // if (name == "background") {
      //   _BG.value = value;
      // }
      // if (name == "color") {
      //   _COLOR.value = value;
      // }
    });
  }
}

function changeColor(picker,style_type) {
  picker.addEventListener("input", function () {
	document.documentElement.style.setProperty(style_type,  picker.value);
    document.cookie = style_type + "=" + picker.value;
  });
}

document.addEventListener("DOMContentLoaded", function () {
  _BG = document.getElementById("backColor");
  _COLOR = document.getElementById("textColor");

  changeColor(_BG, "--bg-color");
  changeColor(_COLOR, "--main-color");
  loadColor();
});


filters.forEach((filter) => {
  filter.addEventListener("click", function () {
    var selected = filter.getAttribute("data-filter");
    if (filter.classList.contains("selected")) {
      filter.classList.remove("selected");
    } else {
      filter.classList.add("selected");
    }
    showHideFonts(selected);
  });
});

function showHideFonts() {
  var selectedFilters = document.querySelectorAll('.filter.selected');
  if (selectedFilters.length > 0) {
    fonts.forEach((font) => { font.classList.add('hidden'); })
    tags.forEach((tag) => {
      var tagValue = tag.getAttribute("data-value");
      selectedFilters.forEach((selectedFilter) => {
        var filterValue = selectedFilter.getAttribute("data-filter");
        if (filterValue == tagValue) {
          tag.closest(".font").classList.remove("hidden");
        } 
      })
    })
  } else {
    fonts.forEach((font) => { font.classList.remove('hidden'); })
  }
}
