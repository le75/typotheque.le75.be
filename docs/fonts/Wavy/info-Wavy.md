---
title: wavy
date_pub: 2025/19/02, 12:05:08
year: 2019
promotion: B1
font_family: wavy
download_url: fonts/Wavy/Wavy.zip
license: SIL Open Font License (OFL) 
license_url: https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL
designers:
 - B2-Lucas Watteau
 - Esa Le 75
styles:
 - Light
paths:
 - fonts/Wavy/wavy.otf
tags:
ascender: 800
descender: -200
sample_text: "J'aime l'idée que le plus grand des sex-symbols new-yorkais n'était qu'un chien dans un foyer de Brazzaville."
scale: sentence # letter, sentence, paragraph
public: yes
---

 