---
title: OnlyTrue
date_pub: 2025/15/01, 14:19:55
year: 2023
promotion: B2
font_family: OnlyTrue
download_url: fonts/Onlytrou/Onlytrou.zip
license: SIL Open Font License (OFL) 
license_url: https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL
designers:
 - B2-Agathe Neyts
 - B2-Alexia Cornil
 - B2-Iris Wolfs
 - B2-Justine Douard
 - Esa Le 75-2023
styles:
 - neutral
 - bigBlob
 - small
paths:
 - fonts/Onlytrou/OnlyTrue-Neutral.ttf
 - fonts/Onlytrou/OnlyTrue-BigBlob.ttf
 - fonts/Onlytrou/OnlyTrue-Small.ttf
tags:
ascender: 800
descender: -200
sample_text: "DES PETITS TROUS, DES PETITS TROUS, ENCORE DES PETITS TROUS."
scale: sentence # letter, sentence, paragraph
public: yes
---

 