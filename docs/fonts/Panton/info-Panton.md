---
title: Panton
date_pub: 2024/11/06, 07:53:03
year: 2018
promotion: B2
font_family: Panton
download_url: fonts/Panton/Panton.zip
license: SIL Open Font License (OFL) 
license_url: https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL
designers:
- B2-Audrey Bukeye
- B2-Alice Lejeune
- Esa Le 75
styles:
 - extra light
paths:
 - fonts/Panton/Panton.otf
tags:
ascender: 700
descender: -300
sample_text: "verner panton avait une prédilection pour tout ce qui permettait de s'asseoir, et utilisait couramment le plastique avec des couleurs vives et des formes généreuses. 1234567890*.🎅 ☘"
scale: sentence # letter, sentence, paragraph
public: yes
---

 