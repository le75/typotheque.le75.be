---
title: Touillette 
date_pub: 2022/09/12, 13:57:53
font_family: Touillette 
download_url: fonts/Touillette/Touillette.zip
license: SIL Open Font License (OFL) 
license_url: 
designers: 
 - B2-Victor Slachmulder
 - B2-Loïs Bonus Plumridge
 - B2-Carlos Lins Santos
 - Esa Le 75
styles:
 - regular
paths:
 - fonts/Touillette/Touillette-Regular.otf
tags: 
 - rounded
 - sans serif
ascender: 800
descender: -200
sample_text: "L'une des modifications les plus simples consiste tout simplement à limer les coins des lettres en bois pour créer une fonte aux formes arrondies. ffffififsftstttaecookie"
scale: sentence # letter, sentence, paragraph
public: yes
---

 
