---
title: revival
date_pub: 2025/06/02, 15:04:58
year: 2020
promotion: B3
font_family: revival
download_url: fonts/rivival/rivival.zip
license: SIL Open Font License (OFL) 
license_url: https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL
designers:
 - B3-Lucie Danloy
 - Esa Le 75
styles:
 - Regular
paths:
 - fonts/rivival/revival.otf
tags:
ascender: 800
descender: -200
sample_text: "y'a un joyeux zwave ko dans le wagon bar qui fume des clopes et boit du the noir."
scale: sentence # letter, sentence, paragraph
public: yes
---

 