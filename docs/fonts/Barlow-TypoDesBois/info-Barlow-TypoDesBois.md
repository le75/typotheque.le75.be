---
title: Barlow-TypoDesBois
date_pub: 2025/25/01, 19:07:39
year: 2019
promotion: B2
font_family: Barlow-TypoDesBois
download_url: fonts/Barlow-TypoDesBois/Barlow-TypoDesBois.zip
license: SIL Open Font License (OFL) 
license_url: https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL
designers:
 - B2-Sofia D. Pimentel
 - B2-Eric Eribon
 - B2-Lucie Danloy
 - B2-Nicoletta Molino
 - Esa Le 75
styles:
 - Regular
paths:
 - fonts/Barlow-TypoDesBois/Barlow-TypoDesBois.otf
tags:
ascender: 1000
descender: -200
sample_text: "LA NATURE TROUVE TOUJOURS SON CHEMIN."
scale: LETTER # letter, sentence, paragraph
public: yes
---

 