---
title: Paradox
date_pub: 2025/15/01, 16:33:46
year: 2024
promotion: B2
font_family: Paradox
download_url: fonts/Paradox/Paradox.zip
license: SIL Open Font License (OFL) 
license_url: https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL
designers:
 - B2-Nico Pellegrini
 - B2-Wiktor Maciejewski
 - B2-Xavier Ruir
 - B2-Juliette Merle
 - B2-Elodie Pierard
 - Esa Le 75

styles:
 - Bold
paths:
 - fonts/Paradox/Paradox.otf
tags:
ascender: 800
descender: -200
sample_text: "ÊTRE OU CHOSE QUI PARAIT DÉFIER LA LOGIQUE PAR SES ASPECTS CONTRADICTOIRES."
scale: sentence-large # letter, sentence, paragraph
public: yes
---

 