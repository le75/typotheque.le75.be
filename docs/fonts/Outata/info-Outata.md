---
title: OUTATA
date_pub: 2025/18/02, 13:39:12
year: 2024
promotion: B2
font_family: PUNCTATA
download_url: fonts/Outata/Outata.zip
license: SIL Open Font License (OFL) 
license_url: https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL
designers:
 - B2-Charel Lepage
 - B2-Sébastiebn Hiernaux
 - B2-Chloé Gobin
 - B2-Adrien Frère
 - Esa Le 75
styles:
 - Outline
paths:
 - fonts/Outata/Outata.otf
tags:
ascender: 800
descender: -200
sample_text: "OUTATA OUT OF PUNCTATA"
scale: letter # letter, sentence, paragraph
public: yes
---

 