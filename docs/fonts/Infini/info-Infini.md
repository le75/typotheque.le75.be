---
title: infini
date_pub: 2025/19/02, 12:04:18
year: 2016
promotion: B2
font_family: infini
download_url: fonts/Infini/Infini.zip
license: SIL Open Font License (OFL) 
license_url: https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL
designers:
 - B2-André Ferreira
 - Esa Le 75
styles:
 - bold
paths:
 - fonts/Infini/infini.otf
tags:
ascender: 860
descender: -140
sample_text: "BOUCLE MEGA INFINI WAOUH DES JOLIS YEUX HIBOU QUI ZAPPENT L'HORIZON."
scale: sentence-large # letter, sentence, paragraph
public: yes
---

 