---
title: Yunik
date_pub: 2024/03/06, 10:13:02
year: 2024
promotion: B2
font_family: Yunik
download_url: fonts/Yunik/Yunik.zip
license: SIL Open Font License (OFL) 
license_url: https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL
designers:
- B2-Hugo Depris
- B2-Nathan Gillet
- B2-Luna Mazzarella
- B2-Léa Hoolans
- Esa Le 75 / 2024
styles:
 - Regular
paths:
 - fonts/Yunik/Yunik.otf
tags:
ascender: 800
descender: -200
sample_text: "Dans les recoins les plus secrets de la forêt, le lichen étend ses bras délicats"
scale: sentence
public: yes
---

 