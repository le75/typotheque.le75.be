---
title: SpaceNotoriousRounded
date_pub: 2025/15/01, 16:15:20
year: 2017
promotion: B2
font_family: SpaceNotoriousRounded
download_url: fonts/SuperNotoriousRounded/SuperNotoriousRounded.zip
license: SIL Open Font License (OFL) 
license_url: https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL
designers:
 - B2-Michaël Dewilder
 - Structure Baton
 - Esa Le 75
styles:
 - Bold
paths:
 - fonts/SuperNotoriousRounded/SpaceNotoriousRounded.otf
tags:
ascender: 1000
descender: -225
sample_text: "Catalogues, affiches, réclames de toutes sortes : croyez-moi, ils contiennent la poésie de notre époque."
scale: sentence # letter, sentence, paragraph
public: yes
---

 