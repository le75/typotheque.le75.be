---
title: Thalsa
date_pub: 2025/19/02, 12:10:00
year:
promotion:
font_family: Thalsa
download_url: fonts/Thalsa/Thalsa.zip
license: SIL Open Font License (OFL) 
license_url: https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL
designers:
 - 
styles:
 - Regular
paths:
 - fonts/Thalsa/ThalsaBook.otf
tags:
ascender: 800
descender: -200
sample_text: "BOUCLE MEGA INFINIE
scale: sentence # letter, sentence, paragraph
public: yes
---

 