---
title: Briz
date_pub: 2025/16/01, 13:23:24
year: 2024
promotion: B2
font_family: Briz
download_url: fonts/Briz/Briz.zip
license: SIL Open Font License (OFL) 
license_url: https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL
designers:
 - B2-Jonatan Ramadan
 - B2-Arthur Longree
 - B2-Lara Gerard
 - B2-Lou Roscoux
 - B2-Roksana Duchnowska
 - Esa Le 75
styles:
 - italic
paths:
 - fonts/Briz/Briz.otf
tags:
ascender: 800
descender: -200
sample_text: "LA BRIZ VIVE DU VENT FLOTTE SUR LES VAGUES, ZÉPHYR ET CYCLONE CHASSENT LES NUAGES."
scale: sentence # letter, sentence, paragraph
public: yes
---

 