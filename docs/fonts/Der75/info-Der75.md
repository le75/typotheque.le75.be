---
title: Der75
date_pub: 2025/04/02, 08:45:58
year: 2016
promotion: B2
font_family: Der75
download_url: fonts/Der75/Der75.zip
license: SIL Open Font License (OFL) 
license_url: https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL
designers:
 - B2-Derreto Sony
 - Esa Le 75
styles:
 - Regular
paths:
 - fonts/Der75/der75.otf
tags:
ascender: 1200
descender: -250
sample_text: "BIM BAM BOUM, CA FAIT PSSSHIT ET CA FAIT VROUM, DANS MA TETE Y'A TOUT QUI TOURNE."
scale: sentence # letter, sentence, paragraph
public: yes
---

 