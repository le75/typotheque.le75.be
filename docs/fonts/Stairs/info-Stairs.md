---
title: Stairs Super Notorious
date_pub: 2025/15/01, 15:43:16
year:
promotion: B2
font_family: Stairs Super Notorious
download_url: fonts/Stairs/Stairs.zip
license: SIL Open Font License (OFL) 
license_url: https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL
designers:
 - B2-Elodie Goldberg
 - Esa Le 75
styles:
 - Regular
paths:
 - fonts/Stairs/Stairs.otf
tags:
ascender: 800
descender: -200
sample_text: "Recite moi l'alphabet."
scale: sentence-large # letter, sentence, paragraph
public: yes
---

 