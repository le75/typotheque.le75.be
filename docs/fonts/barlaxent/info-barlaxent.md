---
title: Barlaxent
year: 2019
promotion: B2
font_family: Barlaxent
download_url: fonts/barlaxent/Barlaxent.otf
license: SIL Open Font License (OFL) 
license_url: https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=ofl
designers:
 - B2-Zephyr Jonnaert 
 - B2-Nassim Gaddari
 - B2-Loana Esperon Vicente
 - Esa Le 75
styles:
 - Regular
paths:
 - fonts/barlaxent/Barlaxent.otf
tags:
 - sans-serif
ascender: 1000
descender: -200
sample_text: Variation de la Barlow proposant une adaptation des signes diacritiques et de la ponctuation afin de revisiter leurs valeurs expressives * Ronan Deriez, Elise Neirinck, Jerome Coche, Stephane De Groef, Pierre Smeets, Romain Marula, Paul Moriau"
scale: paragraph
public: yes
---

 
