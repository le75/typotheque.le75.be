---
title: Marion
date_pub: 2025/29/01, 15:08:26
year: 2023
promotion: B2
font_family: Marion
download_url: fonts/Marion/Marion.zip
license: SIL Open Font License (OFL) 
license_url: https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL
designers:
 - B2-Marion Bouveret
 - Esa Le 75
styles:
 - Regular
paths:
 - fonts/Marion/Marion.otf
tags:
ascender: 800
descender: -200
sample_text: "Du point de vue du caractere meme la qualite du caractere est capitale."
scale: sentence # letter, sentence, paragraph
public: yes
---

 