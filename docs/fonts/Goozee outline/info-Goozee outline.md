---
title: Goozee
date_pub: 2025/04/02, 08:24:54
year: 2020
promotion: B2
font_family: Goozee
download_url: fonts/Goozee outline/Goozee outline.zip
license: SIL Open Font License (OFL) 
license_url: https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL
designers:
 - B2-Alexandra Lambert
 - B2-Madeleine Barbaroux
 - Esa Le 75
styles:
 - outline
paths:
 - fonts/Goozee outline/Goozeeoutline.otf
tags:
ascender: 742
descender: -258
sample_text: "Une matrice est un élément qui fournit un appui une structure et qui sert à entourer reproduire ou construire."
scale: sentence # letter, sentence, paragraph
public: yes
---

 