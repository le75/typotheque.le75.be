---
title: Parasitype
date_pub: 2025/15/01, 14:14:40
year: 2024
promotion: B2
font_family: Parasitype
download_url: fonts/Parasitype/Parasitype.zip
license: SIL Open Font License (OFL) 
license_url: https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL
designers:
 - B2-Nelle Picard
 - B2-Hannah Loose
 - B2-Justyna Zukovska
 - B2-Muskan Jaffer
 - Esa Le 75
styles:
 - Medium
 - ExtraLight
 - Light
 - Bold
 - Regular
 - SemiBold
paths:
 - fonts/Parasitype/Parasitype-Medium.otf
 - fonts/Parasitype/Parasitype-ExtraLight.otf
 - fonts/Parasitype/Parasitype-Light.otf
 - fonts/Parasitype/Parasitype-Bold.otf
 - fonts/Parasitype/Parasitype-Regular.otf
 - fonts/Parasitype/Parasitype-SemiBold.otf
tags:
ascender: 800
descender: -200
sample_text: "AN ORGANISM THAT MULTIPLIES ITSELF INTO MAN OTHERS."
scale: sentence # letter, sentence, paragraph
public: yes
---

 
