---
title: Ellipse
date_pub: 2025/27/01, 13:22:24
year: 2022
promotion: B2
font_family: Ellipse
download_url: fonts/Ellipse/Ellipse.zip
license: SIL Open Font License (OFL) 
license_url: https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL
designers:
 - B2-Julia Snarska
 - Esa Le 75
styles:
 - Regular
paths:
 - fonts/Ellipse/Ellipse.otf
tags:
ascender: 800
descender: -160
sample_text: "J'ai Versé Cinq Yoghourts Aux Kiwis Sur De La Pizza Flambée."
scale: paragraphe # letter, sentence, paragraph
public: yes
---

 