---
title: DingDong
date_pub: 2025/18/02, 19:08:28
year: 2022
promotion: B3
font_family: DingDong
download_url: fonts/Ding Dong/Ding Dong.zip
license: SIL Open Font License (OFL) 
license_url: https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL
designers:
 - B3-Victor Dubien
 - Esa Le 75
styles:
 - Irregular
paths:
 - fonts/Ding Dong/DingDongIrregular.otf
tags:
ascender: 700
descender: -300
sample_text: "Le ciel est clair et l'air encore frais. Par la fenêtre ouverte, triomphe l'été ! DING DANG DONG RINGING AT YOUR BELL DING DANG DONG."
scale: sentence # letter, sentence, paragraph
public: yes
---

 