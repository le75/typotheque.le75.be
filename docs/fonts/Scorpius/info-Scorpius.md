---
title: Scorpius
date_pub: 2024/11/06, 09:36:46
year:
promotion:
font_family: Scorpius
download_url: fonts/Scorpius/Scorpius.zip
license: SIL Open Font License (OFL) 
license_url: https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL
designers:
 - B2-Eva De Luca 
 - B2-Sangye Bay Santos
 - B2-Muskan Jaffer
 - Esa Le 75
styles:
 - regular
paths:
 - fonts/Scorpius/scorpius.otf
tags:
ascender: 800
descender: -200
sample_text: "Le Scorpion est une constellation du zodiaque située dans l'hémisphère céleste sud, près du centre de la Voie lactée, entre la Balance à l'ouest et le Sagittaire à l'est."
scale: sentence # letter, sentence, paragraph
public: yes
---

 