---
title: Frame
date_pub: 2025/10/02, 21:21:32
year:
promotion:
font_family: Frame
download_url: fonts/Frame/Frame.zip
license: SIL Open Font License (OFL) 
license_url: https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL
designers:
 - B2-Laura Cisinski
 - B2-Maxime Verkest
 - Esa Le 75
styles:
 - Regular
paths:
 - fonts/Frame/Frame.otf
tags:
ascender: 800
descender: -200
sample_text: "It turns out machines actually like people *** IS THE CLOUD THE NEW INFINITY ?"
scale: sentence # letter, sentence, paragraph
public: yes
---

 