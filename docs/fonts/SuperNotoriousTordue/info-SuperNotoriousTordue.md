---
title: SuperNotoriousTordue
date_pub: 2025/15/01, 16:18:16
year: 2017
promotion: B2
font_family: SuperNotoriousTordue
download_url: fonts/SuperNotoriousTordue/SuperNotoriousTordue.zip
license: SIL Open Font License (OFL) 
license_url: https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL
designers:
 - B2-Clementine Remy
 - Structure Baton
 - Esa Le 75
styles:
 - thin
paths:
 - fonts/SuperNotoriousTordue/SuperNotoriousTordue.otf
tags:
ascender: 800
descender: -200
sample_text: "Voyez Ce Koala Fou Qui Mange Des Journaux Et Des Photos Dans Un Bungalow."
scale: sentence # letter, sentence, paragraph
public: yes
---

 