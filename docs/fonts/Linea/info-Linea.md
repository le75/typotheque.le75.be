---
title: Linea
date_pub: 2025/15/01, 12:14:13
year: B1
promotion: 2019
font_family: Linea
download_url: fonts/Linea/Linea.zip
license: SIL Open Font License (OFL) 
license_url: https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL
designers: 
- B2-Marie Lescalier
- B2-Marine Sanchez
- Esa Le 75
styles:
 - regular
paths:
 - fonts/Linea/Linea.otf
tags:
ascender: 800
descender: -200
sample_text: "Monsieur Jack, vous dactylographiez bien mieux que Wolf. > @"
scale: sentence-large # letter, sentence, paragraph
public: yes
---

 