---
title: Brutuswriter
date_pub: 2024/11/06, 07:44:23
year: 2019
promotion: B1
font_family: Brutuswriter
download_url: fonts/Brutuswriter/Brutuswriter.zip
license: SIL Open Font License (OFL) 
license_url: https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL
designers:
 - B1-Alexandra Lambert
 - Esa Le 75
styles:
 - Regular
paths:
 - fonts/Brutuswriter/Brutuswriter.otf
tags:
ascender: 800
descender: -200
sample_text: "Chaque époque à eu son expression, sa mode en fonction de la forme de l'outil d'écriture et de son inclinaison sur le support d'inscription. Toutes les écritures calligraphiques au cours des siècles se sont formalisées en jouant sur la variation de ses différents paramètres."
scale: paragraph #paragraph
public: yes
---

 