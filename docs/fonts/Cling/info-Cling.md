---
title: CLING_BOLD
date_pub: 2025/29/01, 14:30:25
year: 2022
promotion: B3
font_family: CLING_BOLD
download_url: fonts/Cling/Cling.zip
license: SIL Open Font License (OFL) 
license_url: https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL
designers:
 - B3-Youmna Zanna
styles:
 - Regular
paths:
 - fonts/Cling/Cling.otf
tags:
ascender: 800
descender: -200
sample_text: "abcdfeghijklmnopqrstuvwxyz"
scale: sentence # letter, sentence, paragraph
public: yes
---

 