---
title: Ductus
date_pub: 2025/27/01, 11:16:27
year: 2019
promotion: B1
font_family: Ductus
download_url: fonts/Ductus/Ductus.zip
license: SIL Open Font License (OFL) 
license_url: https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL
designers:
 - B1-Rachel Degroot
 - Esa Le 75
styles:
 - regular
paths:
 - fonts/Ductus/Ductus.otf
tags:
ascender: 800
descender: -200
sample_text: "En écriture le ductus est l'ordre et la direction, mais aussi la vitesse et le rythme selon lequel on trace les traits qui composent la lettre."
scale: sentence # letter, sentence, paragraph
public: yes
---

 