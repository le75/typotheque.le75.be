---
title: PUNCTATA
date_pub: 2024/31/05, 16:14:22
year: 2023
promotion: B2
font_family: PUNCTATA
download_url: fonts/Punctata/Punctata.zip
license: SIL Open Font License (OFL) 
license_url: https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL
designers:
- B2-Charel Lepage
- B2-Sébastien Hiernaux
- B2-Chloé Gobin
- B2-Adrien Frère
- Esa Le 75
styles:
 - regular
paths:
 - fonts/Punctata/PUNCTATA-Regular.otf
tags:
ascender: 800
descender: -200
sample_text: "LA CAMPANULE PUNCTATA 
OCTOPUS A UN SYSTÈME RACINAIRE VIGOUREUX ET DRAGEONNANT QUI LUI PERMET DE S'INSTALLER RAPIDEMENT ET D'OCCUPER GÉNÉREUSEMENT LE TERRAIN."
scale: sentence-small
public: yes
---

 