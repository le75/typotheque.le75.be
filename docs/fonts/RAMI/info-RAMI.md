---
title: RAMI
date_pub: 2025/15/01, 14:53:35
year: 2023
promotion:
font_family: RAMI
download_url: fonts/RAMI/RAMI.zip
license: SIL Open Font License (OFL) 
license_url: https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL
designers:
 - B2-Justine Douard
 - Esa Le 75
styles:
 - light
paths:
 - fonts/RAMI/RAMI.otf
tags:
ascender: 800
descender: -200
sample_text: "RAMI-FICATION"
scale: letter # letter, sentence, paragraph
public: yes
---

 