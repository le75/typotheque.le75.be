---
title: Haslea
date_pub: 2025/18/02, 13:41:12
year: 2024
promotion: B2
font_family: Haslea
download_url: fonts/Haslea/Haslea.zip
license: SIL Open Font License (OFL) 
license_url: https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL
designers:
 - B2-Arthur Housiaux
 - B2-Jonathan Walendzik
 - B2-Louise Van Raemdonck
 - B2-Noa Verhasselt
 - Esa Le 75
styles:
 - Plain
 - Outline
paths:
 - fonts/Haslea/Haslea-Plain.otf
 - fonts/Haslea/Haslea-Outline.otf
tags:
ascender: 800
descender: -200
sample_text: "HASLEA OU LA CRÉATION D'UNE SYMBIOSE OÙ L'INNOVATION RENCONTRE LE FONCTIONNEL"
scale: SENTENCE # letter, sentence, paragraph
public: yes
---

 