---
title: Barlowfold
date_pub: 2025/15/01, 14:17:39
year: 2019
promotion:
font_family: Barlowfold
download_url: fonts/Barlowfold/Barlowfold.zip
license: SIL Open Font License (OFL) 
license_url: https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL
designers:
 - B1-Victor Dubien
 - B1-Andrea Bonetto
 - Esa Le 75
styles:
 - Bold
paths:
 - fonts/Barlowfold/Barlowfold.otf
tags:
ascender: 800
descender: -200
sample_text: "THIS TYEPFACE HAS BEEN DRAWN BY HAND WITH A LARGE POSCA TOOL."
scale: SENTENCE # letter, sentence, paragraph
public: yes
---

 