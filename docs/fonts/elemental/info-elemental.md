---
title: elemental
date_pub: 2025/29/01, 14:55:56
year: 2024
promotion: B3
font_family: elemental
download_url: fonts/elemental/elemental.zip
license: SIL Open Font License (OFL) 
license_url: https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL
designers:
 - B3-Ana Bogos
 - Esa Le 75
styles:
 - Regular
paths:
 - fonts/elemental/elemental.otf
tags:
ascender: 800
descender: -200
sample_text: "one second after the big bang, the universe consisted of an extremely hot primordial soup of light and particles."
scale: sentence # letter, sentence, paragraph
public: yes
---

 