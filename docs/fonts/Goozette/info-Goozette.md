---
title: Goozette
date_pub: 2025/27/01, 09:39:55
year: 2020
promotion: B2
font_family: Goozette
download_url: fonts/Goozette/Goozette.zip
license: SIL Open Font License (OFL) 
license_url: https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL
designers:
 - B2-Alexandra Lambert
 - B2-Madeleine Barbaroux
 - Esa Le 75
styles:
 - Regular
paths:
 - fonts/Goozette/Goozette.otf
tags:
ascender: 788
descender: -400
sample_text: "Type is a beautiful group of letters not a group of beautiful letters."
scale: sentence # letter, sentence, paragraph
public: yes
---

 