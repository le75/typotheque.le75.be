---
title: SUPER-CLIP
date_pub: 2025/15/01, 15:43:16
year:
promotion:
font_family: SUPER-CLIP
download_url: fonts/SUPERCLIP/SUPERCLIP.zip
license: SIL Open Font License (OFL) 
license_url: https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL
designers:
 - 
styles:
 - CLIP
paths:
 - fonts/SUPERCLIP/SUPER-CLIP.otf
tags:
ascender: 755
descender: -135
sample_text: "Super Clip est une police matricielle ou police d'écran ou police bitmap !."
scale: Sentence # letter, sentence, paragraph
public: yes
---

 