---
title: FLAP
date_pub: 2025/15/01, 16:31:19
year: 2024
promotion: B2
font_family: Flap
download_url: fonts/Flap/Flap.zip
license: SIL Open Font License (OFL) 
license_url: https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL
designers:
 - B2-Lena Beldars
 - B2-Deniz Simsek
 - B2-Goderic Mert
 - B2-Loic Stas de Richelle
 - B2-Elsa Damit
 - Esa Le 75
styles:
 - Regular
paths:
 - fonts/Flap/FLAP.otf
tags:
ascender: 800
descender: -200
sample_text: "ELLE S'ELANCE VERS L'INFINI EVOQUANT À LA FOIS L'ÉLÉVATION ET LE VERTIGE..."
scale: Sentence-large
public: yes
---

 