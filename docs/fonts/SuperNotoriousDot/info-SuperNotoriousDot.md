---
title: SupernotoriousDot
date_pub: 2025/18/02, 21:43:44
year: 2017
promotion: B2
font_family: SupernotoriousDot
download_url: fonts/SuperNotoriousDot/SuperNotoriousDot.zip
license: SIL Open Font License (OFL) 
license_url: https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL
designers:
 - B2-Victor de Ladonchamps
 - Esa Le 75
styles:
 - Bold
paths:
 - fonts/SuperNotoriousDot/SuperNotoriousDot.otf
tags:
ascender: 800
descender: -200
sample_text: "DOT DOT DOT WHO'S THERE"
scale: SENTENCE-LARGE # letter, sentence, paragraph
public: yes
---

 