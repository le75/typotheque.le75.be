---
title: Elastic
year: 2019
promotion: B2
font_family: Barlaxent
font_family: Elastic
download_url: fonts/barlow-elastic/Barlow-elasticc.otf
license: SIL Open Font License (OFL) 
license_url: 
designers:
 - B2-Dries Hamels
 - B2-Olivia Marly
 - B2-Florjent Nuhiji 
 - Esa Le 75
styles:
 - elastic
paths:
 - fonts/barlow-elastic/Barlow-elasticc.otf
tags:
 - sans-serif
 - fantasy
ascender: 1000
descender: -200
sample_text: "Un matériau solide se déforme lorsque des forces lui sont appliquées."
scale: sentence
public: yes
---

 