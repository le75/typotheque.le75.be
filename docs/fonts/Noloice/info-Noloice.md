---
title: Noloice
date_pub: 2025/15/01, 14:17:39
year: 2021
promotion: B1
font_family: Noloice
download_url: fonts/Noloice/Noloice.zip
license: SIL Open Font License (OFL) 
license_url: https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL
designers:
 - B2-Lois Bonus Plumridge
 - B2-Alice Polo
 - B2-Noah Rodriguez
 - Esa Le 75
styles:
 - Regular
paths:
 - fonts/Noloice/Noloice.otf
tags:
ascender: 800
descender: -200
sample_text: "WHAT DO TYPEFACES HAVE TO SAY BEYOND THE WORDS THEY SPELL ?"
scale: sentence # letter, sentence, paragraph
public: yes
---

 