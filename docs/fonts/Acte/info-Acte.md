---
title: Acte
date_pub: 2025/06/02, 15:18:43
year: 2016
promotion: B2
font_family: Acte
download_url: fonts/Acte/Acte.zip
license: SIL Open Font License (OFL) 
license_url: https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL
designers:
 - B2-Sandrine Embreckx
 - Esa Le 75
styles:
 - Regular
paths:
 - fonts/Acte/Acte.otf
tags:
ascender: 700
descender: -200
sample_text: "Le normogra phe est une plaquette dont le contour des lettres a été évidé pour pouvoir les tracer sur le papier."
scale: sentence # letter, sentence, paragraph
public: yes
---

 