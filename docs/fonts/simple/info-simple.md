---
title: simple
date_pub: 2022/01/03, 14:06:26
font_family: simple
download_url: fonts/simple/simple.zip
license: 
license_url: 
designers:
 - B2-Camille Balseau
 - Esa Le 75
styles:
 - regular
paths:
 - fonts/simple/SIMPLE.otf
tags:
 - Sans Serif
ascender: 775
descender: -225
sample_text: "Au XXe siècle certains typographes on cherché à créer le caractère de notre temps' en souhaitant ramener les lettres aux formes géométriques les plus simples possibles. C'était dans l'esprit du temps : en architecture comme dans le design industriel, on cherchait à s'en tenir à un nombre limité d'éléments formels"
scale: paragraph # letter, sentence, paragraph
public: yes
---

 
