---
title: FOURMI
date_pub: 2025/21/02, 10:42:40
year: 2018
promotion: B2
font_family: FOURMI
download_url: fonts/Fourmi/Fourmi.zip
license: SIL Open Font License (OFL) 
license_url: https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL
designers:
 - B2-Nissrine El Moussaoui
 - B2-Mathieu Knoockaert 
 - B2-Ewa Puchalska
 - Esa Le 75
styles:
 - light
paths:
 - fonts/Fourmi/FOURMI-light.otf
tags:
ascender: 800
descender: -200
sample_text: "Surnommée Fourmi En Raison De La Forme Organique Qui La Caractérise, Avec Ses Courbes Parfaitement Dessinées, Sa Taille Etroite Et Ses Pieds Fins, Elle Est Une Icone Du Design Scandinave. 1234567890 ¥ ¦ § ¨ © ª ¬ ­¢ £"
scale: sentence # letter, sentence, paragraph
public: yes
---

 