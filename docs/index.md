---
title: home 
---

Cette typothèque numérique *open source* (téléchargement gratuit) est une bibliothèque dans laquelle est collecté une série de caractères typographiques crées par les étudiant.es de l’orientation graphisme. Chaque année de nouvelles créations voient le jour et alimentent la bibliothèque. Le cours de création de caractères typographiques se donne sous forme de workshop d'une semaine, animé par Flore Van Ryn et un.une intervenant.es invité.e (Structure Baton, Antoine Gelgon, Etienne Ozeray, Laurent Müller, Mariel Niels). Cet apprentissage est envisagé comme une initiation à la pratique : les fontes sont dessinées de façon indirecte selon des approches et protocoles différents et sont encodées manuellement dans le logiciel libre FontForge. Si vous faites usage de l'une de ces fontes, merci de créditer les dessinateur.trices et mettre le lien de la source de publication originale permettant de relier celle-ci au contexte dans lequel elle a été créée.
  

## Credits

This site uses Free Font Library, a tool to manage and display a font collection on the web.

- *Design & Development*: [Luuse](luuse/io)
- *Download*: [Gitlab](gitlab.com/Luuse/Luuse.tools/mkdocks-typotheque)
- *License*: [GNU/GPL](https://www.gnu.org/licenses/gpl.html)
- *Documentation*: luuse.io/freefontlib
- *Featuring*: OpenTypeJs

